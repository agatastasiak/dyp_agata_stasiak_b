package com.example.wykresyPrompt.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.wykresyPrompt.model.UserReport;
import com.example.wykresyPrompt.repository.UserReportRepository;

@RestController
@Component
@RequestMapping("/userReport")
@CrossOrigin(origins = "http://localhost:4200", allowedHeaders="*", maxAge = 9600 )
public class UserReportController {

	@Autowired 
	UserReportRepository userReportRepository;
	
	@GetMapping("/all")
	public List <UserReport> getAllUserReport(){
		return userReportRepository.findAll();
	}
	
	@RequestMapping(value = "/byId", method = RequestMethod.GET)
	@ResponseBody
	public List <UserReport> getUserReportByType(@RequestParam(value="id") Long id){
		return userReportRepository.findUserReportById(id);
	}
	
	@RequestMapping(value = "/byTypeParam", method = RequestMethod.GET)
	@ResponseBody
	public List <UserReport> getUserReportByTypeParam(@RequestParam(value="type") String type){
//		return userReportRepository.findUserReportByType(type);
		return userReportRepository.findUserReportByTypeOrderByTimestampAsc(type);
	}
	
	@GetMapping("/byTypeQuery")
	@ResponseBody
	public List <UserReport> getUserReportByTypeQuery(){
		return userReportRepository.getUserReportByTypeQuery();
	}
		
}
