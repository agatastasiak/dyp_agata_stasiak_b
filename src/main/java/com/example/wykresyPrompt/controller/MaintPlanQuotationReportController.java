package com.example.wykresyPrompt.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.wykresyPrompt.model.MaintPlanQuotationReport;
import com.example.wykresyPrompt.model.UserReport;
import com.example.wykresyPrompt.repository.MaintPlanQuotationReportRepository;
import com.example.wykresyPrompt.repository.UserReportRepository;


@Component
@CrossOrigin(origins = "http://localhost:4200", allowedHeaders="*", maxAge = 9600 )
@RestController
@RequestMapping(("/MaintPlanQuotation"))
public class MaintPlanQuotationReportController {
	
	@Autowired 
	MaintPlanQuotationReportRepository maintPlanQuotationReportRepository;

	@RequestMapping(value = "/byTypeParam", method = RequestMethod.GET)
	@ResponseBody
	public List<MaintPlanQuotationReport> getMaintPlanQuotationReporByTypeParam(@RequestParam(value="type") String type){
		return maintPlanQuotationReportRepository.findMaintPlanQuotationReportByTypeOrderByTimestampAsc(type);
	}
	
}
