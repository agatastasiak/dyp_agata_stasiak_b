package com.example.wykresyPrompt;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WykresyPromptApplication {

	public static void main(String[] args) {
		SpringApplication.run(WykresyPromptApplication.class, args);
	}
}
