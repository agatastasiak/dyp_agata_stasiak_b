package com.example.wykresyPrompt.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.wykresyPrompt.model.MaintPlanQuotationReport;
import com.example.wykresyPrompt.model.UserReport;

public interface MaintPlanQuotationReportRepository extends JpaRepository<MaintPlanQuotationReport,Long>{
	
	public List <MaintPlanQuotationReport> findMaintPlanQuotationReportByTypeOrderByTimestampAsc(String type);
}
