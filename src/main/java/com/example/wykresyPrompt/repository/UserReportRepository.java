package com.example.wykresyPrompt.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.CrossOrigin;

import com.example.wykresyPrompt.model.UserReport;

@Repository
@CrossOrigin(origins = "http://localhost:4200")
public interface UserReportRepository extends JpaRepository<UserReport,Long> {

	public List <UserReport> findUserReportById(Long id);
	@Query(value = "SELECT * FROM USER_REPORT ur WHERE ur.TYPE = 'Total Users'" , nativeQuery = true)
	public List <UserReport> getUserReportByTypeQuery();
	public List <UserReport> findUserReportByType(String type);
	public List <UserReport> findUserReportByTypeOrderByTimestampAsc(String type);
	
	
}
