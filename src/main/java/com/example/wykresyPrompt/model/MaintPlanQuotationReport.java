package com.example.wykresyPrompt.model;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="MAINTPLAN_QUOTATION_REPORT")
public class MaintPlanQuotationReport {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	private int value;
	private String maintenanceresponsible;
	private Date timestamp;
	private String type;
	
	public MaintPlanQuotationReport() {
		super();
	}

	public MaintPlanQuotationReport(Long id, int value, String maintenanceresponsible, Date timestamp, String type) {
		super();
		this.id = id;
		this.value = value;
		this.maintenanceresponsible = maintenanceresponsible;
		this.timestamp = timestamp;
		this.type = type;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public int getValue() {
		return value;
	}

	public void setValue(int value) {
		this.value = value;
	}

	public String getMaintenanceresponsible() {
		return maintenanceresponsible;
	}

	public void setMaintenanceresponsible(String maintenanceresponsible) {
		this.maintenanceresponsible = maintenanceresponsible;
	}

	public Date getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(Date timestamp) {
		this.timestamp = timestamp;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	@Override
	public String toString() {
		return "MaintPlanQuotationReport [id=" + id + ", value=" + value + ", maintenanceresponsible="
				+ maintenanceresponsible + ", timestamp=" + timestamp + ", type=" + type + "]";
	}
	
}
