package com.example.wykresyPrompt.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="USER_REPORT")
public class UserReport{

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	private int value;
	private String role;
	private Date timestamp;
	private String type;
	
	public UserReport() {
		super();
	}

	public UserReport(Long id, int value, String role, Date timestamp, String type) {
		super();
		this.id = id;
		this.value = value;
		this.role = role;
		this.timestamp = timestamp;
		this.type = type;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public int getValue() {
		return value;
	}

	public void setValue(int value) {
		this.value = value;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public Date getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(Date timestamp) {
		this.timestamp = timestamp;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	@Override
	public String toString() {
		return "UserReport [id=" + id + ", value=" + value + ", role=" + role + ", timestamp=" + timestamp + ", type="
				+ type + "]";
	}
}
